import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [{
    path: '/home',
    name: 'home',
    component: Home
  },
  {
    path: '/emailver',
    name: 'emailver',
    component: () => import( /* webpackChunkName: "about" */ '../views/emailver.vue')
  },
  {
    path: '/dash',
    name: 'dash',
    component: () => import( /* webpackChunkName: "about" */ '../views/dash.vue')
  },
  {
    path: '/table',
    name: 'table',
    component: () => import( /* webpackChunkName: "about" */ '../views/table.vue')
  },
  {
    path: '/tab',
    name: 'tab',
    component: () => import( /* webpackChunkName: "about" */ '../views/tab.vue')
  },
  {
    path: '/timer',
    name: 'timer',
    component: () => import( /* webpackChunkName: "about" */ '../views/timer.vue')
  }, 
  {
    path: '/chart',
    name: 'chart',
    component: () => import( /* webpackChunkName: "about" */ '../views/chart.vue')
  },
  {
    path: '/dropzone',
    name: 'dropzone',
    component: () => import( /* webpackChunkName: "about" */ '../views/dropzone.vue')
  },
  {
    path: '/calender',
    name: 'calender',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/calender.vue')
  },
  {
    path: '/adminlogin',
    name: 'adminlogin',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/adminlogin.vue')
  },
  {
    path: '/superadminpage',
    name: 'superadminpage',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/superadminpage.vue')
  },
  {
    path: '/stafflogin',
    name: 'stafflogin',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/stafflogin.vue')
  },
  {
    path: '/forgotpassword',
    name: 'forgotpassword',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/forgotpassword.vue')
  },
  {
    path: '/fpasswordmsg',
    name: 'fpasswordmsg',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/fpasswordmsg.vue')
  },
  {
    path: '/otplogin',
    name: 'otplogin',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/otplogin.vue')
  },
  {
    path: '/feedback',
    name: 'feedback',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/feedback.vue')
  },
  {
    path: '/createcoupon',
    name: 'createcoupon',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/createcoupon.vue')
  },
  {
    path: '/editcoupon',
    name: 'editcoupon',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/editcoupon.vue')
  },
  {
    path: '/couponlist',
    name: 'couponlist',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/couponlist.vue')
  },
  {
    path: '/changepass',
    name: 'changepass',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/changepass.vue')
  },
  {
    path: '/transactiondetails',
    name: 'transactiondetails',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/transactiondetails.vue')
  },
  {
    path: '/vendoraccdetails',
    name: 'vendoraccdetails',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/vendoraccdetails.vue')
  },
  {
    path: '/useraccdetails',
    name: 'useraccdetails',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/useraccdetails.vue')
  },
  {
    path: '/form',
    name: 'form',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/form.vue')
  },
  {
    path: '/formd',
    name: 'formd',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/formd.vue')
  },
  {
    path: '/authenticator',
    name: 'authenticator',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/authenticator.vue')
  },
  {
    path: '/api',
    name: 'api',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/api.vue')
  },
  {
    path: '/error400',
    name: 'error400',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/error400.vue')
  },
  {
    path: '/error401',
    name: 'error401',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/error401.vue')
  },
 
  {
    path: '/error403',
    name: 'error403',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/error403.vue')
  },
  {
    path: '/error404',
    name: 'error404',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/error404.vue')
  },
  {
    path: '/error408',
    name: 'error408',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/error408.vue')
  },
  {
    path: '/error500',
    name: 'error500',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/error500.vue')
  },
  {
    path: '/error501',
    name: 'error501',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/error501.vue')
  },
  {
    path: '/error502',
    name: 'error502',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/error502.vue')
  },
  {
    path: '/error503',
    name: 'error503',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/error503.vue')
  },
  {
    path: '/createstaff',
    name: 'createstaff',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/createstaff.vue')
  },
  {
    path: '/stafflist',
    name: 'stafflist',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/stafflist.vue')
  },
  {
    path: '/wiz',
    name: 'wizard',

    component: () => import( /* webpackChunkName: "about" */ '../views/wizard.vue')
  },
  {
    path: '/gmap',
    name: 'gmap',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/gmap.vue')
  },
  {
    path: '/button',
    name: 'button',

    component: () => import( /* webpackChunkName: "about" */ '../views/button.vue')
  },
  {
    path: '/imgupload',
    name: 'imgupload',
 
    component: () => import( /* webpackChunkName: "about" */ '../views/imageupload.vue')
  },
  {
    path: '/dropdown',
    name: 'dropdown',

    component: () => import( /* webpackChunkName: "about" */ '../views/dropdown.vue')
  },
  {
    path: '/invoice',
    name: 'invoice',

    component: () => import( /* webpackChunkName: "about" */ '../views/invoice.vue')
  },
  {
    path: '/tandc',
    name: 'tandc',

    component: () => import( /* webpackChunkName: "about" */ '../views/termsandconditions.vue')
  },
  {
    path: '/createadmin',
    name: 'createadmin',

    component: () => import( /* webpackChunkName: "about" */ '../views/createadmin.vue')
  },
  {
    path: '/adminlist',
    name: 'adminlist',

    component: () => import( /* webpackChunkName: "about" */ '../views/adminlist.vue')
  },
  {
    path: '/signup',
    name: 'signup',

    component: () => import( /* webpackChunkName: "about" */ '../views/signup.vue')
  },
  {
    path: '/signup-mobile',
    name: 'signup-mobile',

    component: () => import( /* webpackChunkName: "about" */ '../views/signup-mobile.vue')
  },
  {
    path: '/createuseraccount',
    name: 'createuseraccount',

    component: () => import( /* webpackChunkName: "about" */ '../views/createuseraccount.vue')
  },
  {
    path: '/create-vendor-account',
    name: 'create-vendor-account',

    component: () => import( /* webpackChunkName: "about" */ '../views/createvendoraccount.vue')
  },
  {
    path: '/language',
    name: 'language',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/language.vue')
  },
  {
    path: '/pg9',
    name: 'pg9',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg9.vue')
  },
  {
    path: '/pg10',
    name: 'pg10',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg10.vue')
  },
  {
    path: '/pg11',
    name: 'pg11',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg11.vue')
  },
  {
    path: '/pg12',
    name: 'pg12',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg12.vue')
  },
  {
    path: '/pg13',
    name: 'pg13',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg13.vue')
  },
  {
    path: '/pg14',
    name: 'pg14',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg14.vue')
  },
  {
    path: '/pg15',
    name: 'pg15',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg15.vue')
  },
  {
    path: '/pg16',
    name: 'pg16',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg16.vue')
  },
  {
    path: '/pg17',
    name: 'pg17',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg17.vue')
  },
  {
    path: '/pg18',
    name: 'pg18',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg18.vue')
  },
  {
    path: '/pg19',
    name: 'pg19',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg19.vue')
  },
  {
    path: '/pg20',
    name: 'pg20',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg20.vue')
  },
  {
    path: '/pg21',
    name: 'pg21',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg21.vue')
  },
  {
    path: '/pg22',
    name: 'pg22',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg22.vue')
  },
  {
    path: '/pg25',
    name: 'pg25',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg25.vue')
  },
  {
    path: '/pg26',
    name: 'pg26',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg26.vue')
  },
  {
    path: '/pg27',
    name: 'pg27',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg27.vue')
  },
  {
    path: '/pg28',
    name: 'pg28',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg28.vue')
  },
  {
    path: '/pg29',
    name: 'pg29',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg29.vue')
  },
  {
    path: '/pg33',
    name: 'pg33',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg33.vue')
  },
  {
    path: '/pg34',
    name: 'pg34',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg34.vue')
  },
  {
    path: '/pg40',
    name: 'pg40',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg40.vue')
  },
  {
    path: '/pg41',
    name: 'pg41',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg41.vue')
  },
  {
    path: '/pg42',
    name: 'pg42',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg42.vue')
  },
  {
    path: '/pg49',
    name: 'pg49',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg49.vue')
  },
  {
    path: '/pg50',
    name: 'pg50',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg50.vue')
  },
  {
    path: '/pg51',
    name: 'pg51',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg51.vue')
  },
  {
    path: '/pg52',
    name: 'pg52',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg52.vue')
  },
  {
    path: '/pg53',
    name: 'pg53',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg53.vue')
  },
  {
    path: '/pg54',
    name: 'pg54',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg54.vue')
  },
  {
    path: '/pg55',
    name: 'pg55',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg55.vue')
  },
  {
    path: '/pg56',
    name: 'pg56',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg56.vue')
  },
  {
    path: '/pg57',
    name: 'pg57',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg57.vue')
  },
  {
    path: '/pg59',
    name: 'pg59',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg59.vue')
  },
  {
    path: '/pg60',
    name: 'pg60',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg60.vue')
  },
  {
    path: '/pg61',
    name: 'pg61',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg61.vue')
  },
  {
    path: '/pg63',
    name: 'pg63',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg63.vue')
  },
  {
    path: '/pg64',
    name: 'pg64',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg64.vue')
  },
  {
    path: '/pg65',
    name: 'pg65',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg65.vue')
  },
  {
    path: '/pg66',
    name: 'pg66',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg66.vue')
  },
  {
    path: '/pg67',
    name: 'pg67',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg67.vue')
  },
  {
    path: '/pg68',
    name: 'pg68',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg68.vue')
  },
  {
    path: '/pg69',
    name: 'pg69',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg69.vue')
  },
  {
    path: '/pg70',
    name: 'pg70',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg70.vue')
  },
  {
    path: '/pg71',
    name: 'pg71',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg71.vue')
  },
  {
    path: '/pg72',
    name: 'pg72',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg72.vue')
  },
  {
    path: '/pg73',
    name: 'pg73',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg73.vue')
  },
  {
    path: '/pg74',
    name: 'pg74',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg74.vue')
  },
  {
    path: '/pg75',
    name: 'pg75',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg75.vue')
  },
  {
    path: '/pg76',
    name: 'pg76',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg76.vue')
  },
  {
    path: '/pg77',
    name: 'pg77',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg77.vue')
  },
  {
    path: '/pg78',
    name: 'pg78',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg78.vue')
  },
  {
    path: '/pg79',
    name: 'pg79',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg79.vue')
  },
  {
    path: '/pg80',
    name: 'pg80',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg80.vue')
  },
  {
    path: '/pg81',
    name: 'pg81',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg81.vue')
  },
  {
    path: '/pg82',
    name: 'pg82',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg82.vue')
  },
  {
    path: '/pg83',
    name: 'pg83',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg83.vue')
  },
  {
    path: '/pg85',
    name: 'pg85',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg85.vue')
  },
  {
    path: '/pg96',
    name: 'pg96',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg96.vue')
  },
  {
    path: '/pg97',
    name: 'pg97',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg97.vue')
  },
  {
    path: '/pg98',
    name: 'pg98',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg98.vue')
  },
  {
    path: '/pg99',
    name: 'pg99',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg99.vue')
  },
  {
    path: '/pg100',
    name: 'pg100',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg100.vue')
  },
  {
    path: '/pg101',
    name: 'pg101',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg101.vue')
  },
  {
    path: '/pg102',
    name: 'pg102',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg102.vue')
  },
  {
    path: '/pg103',
    name: 'pg103',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg103.vue')
  },
  {
    path: '/pg104',
    name: 'pg104',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg104.vue')
  },
  {
    path: '/pg108',
    name: 'pg108',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg108.vue')
  },
  {
    path: '/pg109',
    name: 'pg109',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg109.vue')
  },
  {
    path: '/pg110',
    name: 'pg110',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg110.vue')
  },
  {
    path: '/pg111',
    name: 'pg111',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg111.vue')
  },
  {
    path: '/pg112',
    name: 'pg112',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg112.vue')
  },
  {
    path: '/pg113',
    name: 'pg113',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg113.vue')
  },
  {
    path: '/pg114',
    name: 'pg114',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg114.vue')
  },
  {
    path: '/pg115',
    name: 'pg115',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg115.vue')
  },
  {
    path: '/pg116',
    name: 'pg116',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg116.vue')
  },
  {
    path: '/pg117',
    name: 'pg117',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg117.vue')
  },
  {
    path: '/pg118',
    name: 'pg118',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg118.vue')
  },
  {
    path: '/pg119',
    name: 'pg119',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg119.vue')
  },
  {
    path: '/pg132',
    name: 'pg132',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg132.vue')
  },
  {
    path: '/pg141',
    name: 'pg141',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg141.vue')
  },
  {
    path: '/pg143',
    name: 'pg143',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg143.vue')
  },
  {
    path: '/pg144',
    name: 'pg144',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg144.vue')
  },
  {
    path: '/pg147',
    name: 'pg147',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg147.vue')
  },
  {
    path: '/pg148',
    name: 'pg148',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg148.vue')
  },
  {
    path: '/pg149',
    name: 'pg149',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg149.vue')
  },
  {
    path: '/pg150',
    name: 'pg150',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg150.vue')
  },
  {
    path: '/pg151',
    name: 'pg151',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg151.vue')
  },
  {
    path: '/pg152',
    name: 'pg152',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg152.vue')
  },
  {
    path: '/pg153',
    name: 'pg153',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg153.vue')
  },
  {
    path: '/pg154',
    name: 'pg154',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg154.vue')
  },
  {
    path: '/pg155',
    name: 'pg155',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg155.vue')
  },
  {
    path: '/pg156',
    name: 'pg156',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg156.vue')
  },
  {
    path: '/pg157',
    name: 'pg157',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg157.vue')
  },
  {
    path: '/pg158',
    name: 'pg158',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg158.vue')
  },
  {
    path: '/pg159',
    name: 'pg159',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg159.vue')
  },
  {
    path: '/pg160',
    name: 'pg160',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg160.vue')
  },
  {
    path: '/pg168',
    name: 'pg168',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg168.vue')
  },
  {
    path: '/pg169',
    name: 'pg169',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg169.vue')
  },
  {
    path: '/pg170',
    name: 'pg170',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg170.vue')
  },
  {
    path: '/pg171',
    name: 'pg171',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg171.vue')
  },
  {
    path: '/pg172',
    name: 'pg172',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg172.vue')
  },
  {
    path: '/pg173',
    name: 'pg173',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg173.vue')
  },
  {
    path: '/pg174',
    name: 'pg174',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg174.vue')
  },
  {
    path: '/pg175',
    name: 'pg175',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg175.vue')
  },
  {
    path: '/pg176',
    name: 'pg176',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg176.vue')
  },
  {
    path: '/pg178',
    name: 'pg178',
    
    component: () => import( /* webpackChunkName: "about" */ '../views/pg178.vue')
  }
]

const router = new VueRouter({
  routes
}) 

export default router
