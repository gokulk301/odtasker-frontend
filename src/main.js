import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import * as VueGoogleMaps from 'vue2-google-maps'
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import VImageInput from 'vuetify-image-input/a-la-carte';
 
Vue.component('VImageInput', VImageInput);


import VueGoogleCharts from 'vue-google-charts'
import CKEditor from '@ckeditor/ckeditor5-vue';
import moment from 'moment'
Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('dddd, MMMM Do YYYY, hh:mm a')
  }
});

Vue.use( CKEditor );


Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
//charts
Vue.use(VueGoogleCharts)

// wizard
Vue.use(VueFormWizard)

Vue.use(vue2Dropzone)
 

// googlemaps
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDYLq0eB8RcgYeG8qJoVPA4DkNNihnu7sc',
    libraries: 'places', 
  },
  
 
 
})
//imageupload
Vue.component('VImageInput', VImageInput);



Vue.config.productionTip = false

new Vue({
  router: router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
